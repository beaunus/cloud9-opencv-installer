#/bin/bash

# Navigate to top directory
cd ~/workspace

# Get the most recent opencv V2 zip
# This is the version compatible with the npm opencv library
wget https://github.com/Itseez/opencv/archive/2.4.12.zip

# Unzip the compressed files to the current directory
unzip 2.4.12.zip

# Remove .zip
rm 2.4.12.zip

# Navigate into the unzipped files and create build directories
cd opencv-2.4.12
mkdir release
cd release

# Build the cmake without tests/samples/cuda, etc. and complete build
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D WITH_CUDA=OFF -D BUILD_opencv_apps=OFF -D BUILD_DOCS=OFF -D BUILD_EXAMPLES=OFF -D BUILD_PERF_TESTS=OFF -D BUILD_TESTS=OFF -D ENABLE_PRECOMPILED_HEADERS=OFF -D BUILD_opencv_gpu=OFF -D WITH_OPENCL=OFF ..
make
sudo make install

echo "Now run 'npm install opencv' in your project folder"

exit